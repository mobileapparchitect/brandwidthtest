package com.mobileparadigm.arkangel.bradwidth_backend_module.builders;

import android.text.TextUtils;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.mobileparadigm.arkangel.bradwidth_backend_module.CatsMobileApplication;
import com.mobileparadigm.arkangel.bradwidth_backend_module.logs.Log;
import com.mobileparadigm.arkangel.bradwidth_backend_module.network.ErrorResponse;
import com.mobileparadigm.arkangel.bradwidth_backend_module.network.HttpsTrustManager;
import com.mobileparadigm.arkangel.bradwidth_backend_module.network.ServicesUtils;
import com.mobileparadigm.arkangel.bradwidth_backend_module.network.VolleySingleton;
import com.mobileparadigm.arkangel.bradwidth_backend_module.utils.DeviceUtils;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class CatsApiRequest<T> extends Request<T> {

    protected static final int SOCKET_TIMEOUT_MS = 15000;
    private final Class<T> responseType;
    private final Response.Listener<T> listener;
    private final Map<String, String> params;
    private final boolean allowFromCache;

    private final MultipartEntityBuilder builder;
    private ObjectMapper objectMapper;
    private HttpEntity entity;

    public CatsApiRequest(String url, int method, Response.Listener listener, Class responseType,
                          Response.ErrorListener errorListener, HashMap<String, String> params, String tag, boolean allowFromCache) {
        super(method, url, new ErrorHandler(errorListener, url));
        super.setTag(tag);
        this.responseType = responseType;
        this.listener = listener;
        this.params = params;
        this.allowFromCache = allowFromCache;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        this.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        setShouldCache(false);
        this.builder = null;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();
        String authToken = CatsMobileApplication.getApiKey();
        if (!TextUtils.isEmpty(authToken)) {
            String userpass = authToken + ":" + "X";
            String basicAuth = "Basic " + Base64.encodeToString(userpass.getBytes(), Base64.CRLF);
            if (authToken != null) {
                headers.put(HeaderFields.AUTHORIZATION, basicAuth);
            }
        }
        return headers;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (getMethod() == Method.POST) {
            return params;
        } else {
            return new HashMap<>();
        }
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String jsonString;
        if (response.headers.containsKey(HeaderFields.CONTENT_ENCODING) && response.headers.get(HeaderFields.CONTENT_ENCODING).equals(HeaderFields.ENCODING_GZIP)) {
            try {
                jsonString = ServicesUtils.getGzipString(response);
            } catch (IOException e) {
                e.printStackTrace();
                return Response.error(new ParseError(e));
            }
        } else {
            try {
                jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return Response.error(new ParseError(e));
            }
        }
        try {
            jsonString = ServicesUtils.getDataString(jsonString);
            T object = objectMapper.readValue(jsonString, responseType);
            // T object = LoganSquare.parse(jsonString, responseType);
            Log.i("responseType:" + responseType + " object type:" + object.getClass().getSimpleName());
            return Response.success(object,
                    ServicesUtils.parseIgnoreCacheHeaders(response));
        } catch (Exception e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        try {
            String response = new String(volleyError.networkResponse.data,
                    HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
            return objectMapper.readValue(response, ErrorResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.parseNetworkError(volleyError);
    }

    @Override
    protected void deliverResponse(T t) {
        listener.onResponse(t);
    }

    public void execute() {
        if (getUrl().startsWith("https")) {
            HttpsTrustManager.allowAllSSL();
        }
        if (allowFromCache) {
            Cache cache = VolleySingleton.getInstance().getRequestQueue().getCache();
            Cache.Entry entry = cache.get(getUrl());
            if (entry != null && !DeviceUtils.isConnected(CatsMobileApplication.getAppContext())) {
                try {
                    String data = new String(entry.data, Charsets.UTF_8.name());
                    listener.onResponse(objectMapper.readValue(data, responseType));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        VolleySingleton.getInstance().getRequestQueue().cancelAll(getTag());
        VolleySingleton.getInstance().getRequestQueue().add(this);
    }

    private static class ErrorHandler implements Response.ErrorListener {

        private Response.ErrorListener errorListener;
        private String url;

        public ErrorHandler(Response.ErrorListener errorListener, String url) {
            this.errorListener = errorListener;
            this.url = url;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            if (errorListener != null)
                errorListener.onErrorResponse(error);
        }
    }
}
