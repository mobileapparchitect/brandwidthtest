package com.mobileparadigm.arkangel.bradwidth_backend_module.model;

import java.util.ArrayList;
import android.os.Parcelable;
import java.util.List;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CatDataContainer implements Parcelable{

    private static final String FIELD_FOOD = "food";
    private static final String FIELD_CATS = "cats";



    @JsonProperty("food")
    private List<Food> mFoods;

    @JsonProperty("cats")
    private List<Cat> mCats;


    public CatDataContainer(){

    }

    public void setFoods(List<Food> foods) {
        mFoods = foods;
    }

    public List<Food> getFoods() {
        return mFoods;
    }

    public void setCats(List<Cat> cats) {
        mCats = cats;
    }

    public List<Cat> getCats() {
        return mCats;
    }

    public CatDataContainer(Parcel in) {
        mFoods = new ArrayList<Food>();
        in.readTypedList(mFoods, Food.CREATOR);
        mCats = new ArrayList<Cat>();
        in.readTypedList(mCats, Cat.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CatDataContainer> CREATOR = new Creator<CatDataContainer>() {
        public CatDataContainer createFromParcel(Parcel in) {
            return new CatDataContainer(in);
        }

        public CatDataContainer[] newArray(int size) {
        return new CatDataContainer[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mFoods);
        dest.writeTypedList(mCats);
    }


}