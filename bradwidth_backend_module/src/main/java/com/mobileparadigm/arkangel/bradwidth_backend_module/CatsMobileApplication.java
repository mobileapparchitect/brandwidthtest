package com.mobileparadigm.arkangel.bradwidth_backend_module;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.provider.Settings;

import com.mobileparadigm.arkangel.bradwidth_backend_module.logs.Log;
import com.mobileparadigm.arkangel.bradwidth_backend_module.logs.LogConfig;
import com.mobileparadigm.arkangel.bradwidth_backend_module.network.VolleySingleton;

import java.util.HashSet;

public class CatsMobileApplication extends Application {

    protected static CatsMobileApplication singleton;


    public static CatsMobileApplication getInstance() {
        return singleton;
    }

    public static String getDeviceId() {
        return Settings.Secure.getString(singleton.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getApiKey() {
        return "";
    }

    public static Context getAppContext() {
        return singleton.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        initLog();
    }

    public void initLog() {
        Log.init(this, new LogConfig() {
            @Override
            public byte getLogLevel() {
                return Log.ALL;
            }

            @Override
            public HashSet<Class<?>> getClassesToMute() {
                return null;
            }

            @Override
            public HashSet<String> getPackagesToMute() {
                return null;
            }
        });
    }

    @Override
    public void onLowMemory() {
        Runtime.getRuntime().gc();
        Log.e("onLowMemory()");
        VolleySingleton.getInstance().cancelAll();
        super.onLowMemory();
    }

    public boolean isDebug() {
        return (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }

    public String getAppBaseUrl() {
        return "https://gist.githubusercontent.com";
    }
}
