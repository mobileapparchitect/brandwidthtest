package com.mobileparadigm.arkangel.bradwidth_backend_module.builders;


import com.android.volley.Request;
import com.mobileparadigm.arkangel.bradwidth_backend_module.network.ServicesUtils;

public  class GetRequestBuilder extends BaseRequestBuilder {
        public GetRequestBuilder() {
            setMethod(Request.Method.GET);
        }

        @Override
        public CatsApiRequest build() {
            encodeParams();
            checkPreconditions();
            return new CatsApiRequest(url, method, listener, responseType, errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);
        }
    }