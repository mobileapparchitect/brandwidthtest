package com.mobileparadigm.arkangel.bradwidth_backend_module.builders;

public class CatDataGetRequestBuilder extends GetRequestBuilder {


    public CatDataGetRequestBuilder(String username, String hashOne, boolean raw, String hashTwo) {

        super();
        StringBuilder builder = new StringBuilder();

        builder.append("/");
        builder.append(username);
        builder.append("/");
        builder.append(hashOne); builder.append("/");
        if(raw){
            builder.append("raw");
            builder.append("/");
        }
        builder.append(hashTwo);


        url += builder.toString() + RequestMethods.METHOD_CATS;
    }

    public static CatDataGetRequestBuilder newBuilder(String username, String hashOne, boolean raw, String hashTwo) {
        return new CatDataGetRequestBuilder(username, hashOne, raw,hashTwo);
    }
}
