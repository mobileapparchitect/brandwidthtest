package com.mobileparadigm.arkangel.bradwidth_backend_module.model;

import android.os.Parcelable;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;
public class Cat implements Parcelable{


    @JsonProperty("size")
    private String mSize;

    @JsonProperty("colour")
    private String mColour;

    @JsonProperty("whiskers")
    private int mWhisker;

    @JsonProperty("legs")
    private int mLeg;

    @JsonProperty("prefered-food")
    private String mPreferedFood;

    @JsonProperty("breed")
    private String mBreed;

    @JsonProperty("image")
    private Image mImage;


    public Cat(){

    }

    public void setSize(String size) {
        mSize = size;
    }

    public String getSize() {
        return mSize;
    }

    public void setColour(String colour) {
        mColour = colour;
    }

    public String getColour() {
        return mColour;
    }

    public void setWhisker(int whisker) {
        mWhisker = whisker;
    }

    public int getWhisker() {
        return mWhisker;
    }

    public void setLeg(int leg) {
        mLeg = leg;
    }

    public int getLeg() {
        return mLeg;
    }

    public void setPreferedFood(String preferedFood) {
        mPreferedFood = preferedFood;
    }

    public String getPreferedFood() {
        return mPreferedFood;
    }

    public void setBreed(String breed) {
        mBreed = breed;
    }

    public String getBreed() {
        return mBreed;
    }

    public void setImage(Image image) {
        mImage = image;
    }

    public Image getImage() {
        return mImage;
    }

    public Cat(Parcel in) {
        mSize = in.readString();
        mColour = in.readString();
        mWhisker = in.readInt();
        mLeg = in.readInt();
        mPreferedFood = in.readString();
        mBreed = in.readString();
        mImage = in.readParcelable(Image.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Cat> CREATOR = new Creator<Cat>() {
        public Cat createFromParcel(Parcel in) {
            return new Cat(in);
        }

        public Cat[] newArray(int size) {
        return new Cat[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mSize);
        dest.writeString(mColour);
        dest.writeInt(mWhisker);
        dest.writeInt(mLeg);
        dest.writeString(mPreferedFood);
        dest.writeString(mBreed);
        dest.writeParcelable(mImage, flags);
    }


}