package com.mobileparadigm.arkangel.bradwidth_backend_module.model;

import android.os.Parcelable;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Image implements Parcelable{



    @JsonProperty("mHdpi")
    private String mHdpi;

    @JsonProperty("mMdpi")
    private String mMdpi;

    @JsonProperty("mXhdpi")
    private String mXhdpi;

    @JsonProperty("mLdpi")
    private String mLdpi;

    @JsonProperty("mXxhdpi")
    private String mXxhdpi;


    public Image(){

    }

    public void setHdpi(String hdpi) {
        mHdpi = hdpi;
    }

    public String getHdpi() {
        return mHdpi;
    }

    public void setMdpi(String mdpi) {
        mMdpi = mdpi;
    }

    public String getMdpi() {
        return mMdpi;
    }

    public void setXhdpi(String xhdpi) {
        mXhdpi = xhdpi;
    }

    public String getXhdpi() {
        return mXhdpi;
    }

    public void setLdpi(String ldpi) {
        mLdpi = ldpi;
    }

    public String getLdpi() {
        return mLdpi;
    }

    public void setXxhdpi(String xxhdpi) {
        mXxhdpi = xxhdpi;
    }

    public String getXxhdpi() {
        return mXxhdpi;
    }

    public Image(Parcel in) {
        mHdpi = in.readString();
        mMdpi = in.readString();
        mXhdpi = in.readString();
        mLdpi = in.readString();
        mXxhdpi = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        public Image[] newArray(int size) {
        return new Image[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mHdpi);
        dest.writeString(mMdpi);
        dest.writeString(mXhdpi);
        dest.writeString(mLdpi);
        dest.writeString(mXxhdpi);
    }


}