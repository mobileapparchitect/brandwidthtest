package com.mobileparadigm.arkangel.bradwidth_backend_module.model;

import android.os.Parcelable;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Food implements Parcelable{



    @JsonProperty("package")
    private String mPackage;
    @JsonProperty("name")
    private String mName;


    public Food(){

    }



    public String getPackage() {
        return mPackage;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public Food(Parcel in) {
        mPackage = in.readString();
        mName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Food> CREATOR = new Creator<Food>() {
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        public Food[] newArray(int size) {
        return new Food[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPackage);
        dest.writeString(mName);
    }


}