package com.mobileparadigm.arkangel.bradwidth_backend_module;

import com.mobileparadigm.arkangel.bradwidth_backend_module.utils.DeviceUtils;

public class FailedRequestException extends Exception {

    public static final int SERVER_EXCEPTION = 500;
    public static final int SERVER_BAD_REQUEST = 400;
    public static final int MALFORMED_URL = -901;
    public static final int PROTOCOL_ERROR = -902;
    public static final int IO_ERROR = -903;
    public static final int ENCODE_REQUEST_ERROR = -904;
    public static final int DECODE_RESPONSE_ERROR = -905;

    private int mCode;

    public FailedRequestException(int code, String message) {
        super(message);
        mCode = code;
    }

    public int getCode() {
        return mCode;
    }

    @Override
    public boolean equals(Object o) {
        FailedRequestException that = (FailedRequestException) o;
        return (this.mCode == that.mCode)
                && DeviceUtils.equals(this.getMessage(), that.getMessage());
    }
}
