package com.mobileparadigm.arkangel.bradwidth_backend_module;


public interface HTTPConnection {
    public HTTPResponse execute(String url, RequestType requestType, Header[] headers) throws FailedRequestException;
    public HTTPResponse execute(String url, RequestType requestType, byte[] requestBody, Header[] headers) throws FailedRequestException;
}