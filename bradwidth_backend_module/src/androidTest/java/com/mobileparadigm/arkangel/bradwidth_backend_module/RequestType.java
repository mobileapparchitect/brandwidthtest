package com.mobileparadigm.arkangel.bradwidth_backend_module;

public enum RequestType {
    GET,
    POST,
    PUT,
    DELETE
}