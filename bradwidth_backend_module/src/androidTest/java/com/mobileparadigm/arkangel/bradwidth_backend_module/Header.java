package com.mobileparadigm.arkangel.bradwidth_backend_module;

import com.mobileparadigm.arkangel.bradwidth_backend_module.utils.DeviceUtils;

public class Header {
    private String mKey;
	private String mName;

	public String getKey() {
		return mKey;
	}

	public String getName() {
		return mName;
	}

    public Header(String key, String name) {
        mKey = key;
        mName = name;
    }

    @Override
    public boolean equals(Object o) {
        Header that = (Header) o;
        return DeviceUtils.equals(this.mKey, that.mKey)
                && DeviceUtils.equals(this.mName, that.mName);
    }

    @Override
    public String toString() {
        return "Header[key=" + mKey
                + ";name=" + mName
                + "]";
    }
}