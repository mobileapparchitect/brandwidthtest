package com.mobileparadigm.arkangel.bradwidth_backend_module;

public class HTTPResponse {
    private int mHttpStatusCode;
	private Header[] mHeaders;
    private String mBody;

    public void setHttpStatusCode(int newVal) {
        mHttpStatusCode = newVal;
    }

    public int getHttpStatusCode() {
        return mHttpStatusCode;
    }

	public void setHeaders(Header[] headers) {
		mHeaders = headers;
	}

	public Header[] getHeaders() {
		return mHeaders;
	}

    public String getBody() {
        return mBody;
    }

    public void setBody(String newVal) {
        mBody = newVal;
    }

    public boolean isValid() {
        return (mHttpStatusCode >= 200 && mHttpStatusCode < 299);
    }
}