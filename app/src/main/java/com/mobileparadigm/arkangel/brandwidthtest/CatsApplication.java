package com.mobileparadigm.arkangel.brandwidthtest;

import com.mobileparadigm.arkangel.bradwidth_backend_module.CatsMobileApplication;

public class CatsApplication extends CatsMobileApplication {

    private static CatsApplication singleton;

    public static CatsApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}
