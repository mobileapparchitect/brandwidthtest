package com.mobileparadigm.arkangel.brandwidthtest.adapters;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mobileparadigm.arkangel.bradwidth_backend_module.model.Cat;
import com.mobileparadigm.arkangel.brandwidthtest.R;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class StickyListCatsAdapter extends BaseAdapter implements View.OnClickListener, StickyListHeadersAdapter, SectionIndexer {

    private Context mContext;

    private LayoutInflater mInflater;

    private Character[] rows;
    private int[] mSectionIndices;
    private ArrayList<Long> listOfheaders;
    private ArrayList<Cat> listOfCakes;

    public StickyListCatsAdapter(Context context) {
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return listOfCakes.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfCakes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void allTitleToUppercase() {
        for (Cat cat : listOfCakes) {
            cat.setBreed(cat.getBreed().toUpperCase());
        }
    }

    public void addAll(ArrayList<Cat> items) {
        listOfCakes = items;
        allTitleToUppercase();
        mSectionIndices = getSectionIndices();
        rows = getSectionLetters();
        listOfheaders = new ArrayList<>();
        setUpHeaders(listOfCakes);
        notifyDataSetChanged();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public void setUpHeaders(ArrayList<Cat> parseObjectArrayList) {
        for (int count = 0; count < parseObjectArrayList.size(); count++) {
            Cat cat = (Cat) parseObjectArrayList.get(count);
            long hashCode = (long) cat.getBreed().hashCode();
            listOfheaders.add(hashCode);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.view_cat_row, null);
            holder = new ViewHolder();
            holder.breed = (TextView) convertView.findViewById(R.id.breed);
            holder.legs = (TextView) convertView.findViewById(R.id.number_of_legs);
            holder.imageView = (ImageView) convertView.findViewById(R.id.image_view);
            holder.preferredFood = (TextView) convertView.findViewById(R.id.preferred_food);
            holder.colour = (TextView) convertView.findViewById(R.id.cat_colour);
            holder.size = (TextView) convertView.findViewById(R.id.size);
            holder.whiskers = (TextView) convertView.findViewById(R.id.number_of_whiskers);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Cat cat = (Cat) getItem(position);
        holder.breed.setText(cat.getBreed());
        holder.colour.setText(cat.getColour());
        holder.whiskers.setText(String.valueOf(cat.getWhisker()));
        holder.legs.setText(String.valueOf(cat.getLeg()));
        holder.size.setText(cat.getSize());
        holder.preferredFood.setText(cat.getPreferedFood());
        if(!TextUtils.isEmpty(cat.getColour() )){
       //        holder.colour.setTextColor(Color.parseColor(cat.getColour()));
        }

        if (!TextUtils.isEmpty(cat.getImage().getXxhdpi())) {
            Glide.with(mContext)
                  //  .load(Uri.parse("file:///android_asset/" + cat.getImage().getXxhdpi()))
                    .load("http://res.cloudinary.com/mobile-paradigm/image/upload/v1438732627/NaijaStartup/brandwidth/" + cat.getImage().getXxhdpi() +".jpg")
                            .into(holder.imageView);
        }
        return convertView;
    }

    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        char lastFirstChar = ((Cat) getItem(0)).getBreed().charAt(0);
        sectionIndices.add(0);
        for (int i = 1; i < listOfCakes.size(); i++) {
            if (listOfCakes.get(i).getBreed().charAt(0) != lastFirstChar) {
                lastFirstChar = listOfCakes.get(i).getBreed().charAt(0);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup parent) {
        HeaderViewHolder headerViewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.header_layout, parent, false);
            headerViewHolder = new HeaderViewHolder();
            headerViewHolder.headerView = (TextView) convertView.findViewById(R.id.header);
            headerViewHolder.headerView.setClickable(false);
            convertView.setTag(headerViewHolder);
        } else {
            headerViewHolder = (HeaderViewHolder) convertView.getTag();
        }
        CharSequence headerChar = listOfCakes.get(i).getBreed().subSequence(0, 1);
        headerViewHolder.headerView.setText(headerChar);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return listOfheaders.get(position);
    }

    @Override
    public Object[] getSections() {
        return rows;
    }

    @Override
    public int getPositionForSection(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    private Character[] getSectionLetters() {
        Character[] letters = new Character[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = listOfCakes.get(i).getBreed().charAt(0);
        }
        return letters;
    }

    @Override
    public int getSectionForPosition(int section) {
        if (mSectionIndices.length == 0) {
            return 0;
        }
        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    @Override
    public void onClick(View view) {
    }

    public class ViewHolder {

        TextView breed;
        TextView legs;
        ImageView imageView;
        TextView preferredFood;
        TextView colour;
        TextView size;
        TextView whiskers;

    }

    public class HeaderViewHolder {

        TextView headerView;
    }
}
