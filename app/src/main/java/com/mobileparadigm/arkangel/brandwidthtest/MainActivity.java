package com.mobileparadigm.arkangel.brandwidthtest;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.mobileparadigm.arkangel.brandwidthtest.ui.CatsListDataFragment;


public class MainActivity extends FragmentActivity {

    private android.support.v4.app.FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_container, CatsListDataFragment.getInstance()).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
