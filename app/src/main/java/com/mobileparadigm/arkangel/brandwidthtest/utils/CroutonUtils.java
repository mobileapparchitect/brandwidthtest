package com.mobileparadigm.arkangel.brandwidthtest.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;

import com.mobileparadigm.arkangel.brandwidthtest.R;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class CroutonUtils {

    public static final int DURATION_INFINITE = -1;
    private static final int DURATION_PER_CHAR = 35;
    private static final String LOG_TAG = "utils.CroutonUtils";

    public static Crouton noInternet(final Activity context) {
        final int padding = (int) context.getResources().getDimension(R.dimen.croutonPadding);
        final int errorMsg = R.string.no_connectivity;
        Crouton crouton = Crouton.makeText(context,
                errorMsg,
                new Style.Builder().setConfiguration(new Configuration.Builder().setDuration(DURATION_INFINITE).build())
                        .setBackgroundColorValue(Color.RED)
                        .setPaddingInPixels(padding)
                        .setHeight(LayoutParams.WRAP_CONTENT)
                        .build()
        );
        crouton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
            }
        });
        crouton.show();
        return crouton;
    }

    public static Crouton error(final Activity context, String errorMsg) {
        final int padding = (int) context.getResources().getDimension(R.dimen.croutonPadding);
        final int duration = DURATION_PER_CHAR * errorMsg.length();
        Crouton crouton = Crouton.makeText(context,
                errorMsg,
                new Style.Builder().setConfiguration(new Configuration.Builder().setDuration(duration).build())
                        .setBackgroundColorValue(Color.RED)
                        .setPaddingInPixels(padding)
                        .setHeight(LayoutParams.WRAP_CONTENT)
                        .build()
        );
        crouton.show();
        return crouton;
    }

    public static Crouton error(final Activity context, int errorMsg) {
        final int padding = (int) context.getResources().getDimension(R.dimen.croutonPadding);
        final int duration = DURATION_PER_CHAR * context.getResources().getText(errorMsg).length();
        Crouton crouton = Crouton.makeText(context,
                errorMsg,
                new Style.Builder().setConfiguration(new Configuration.Builder().setDuration(duration).build())
                        .setBackgroundColorValue(Color.RED)
                        .setHeight(LayoutParams.WRAP_CONTENT)
                        .build()
        );
        crouton.show();
        return crouton;
    }

    public static Crouton info(final Activity context, int msg) {
        final int padding = (int) context.getResources().getDimension(R.dimen.croutonPadding);
        final int duration = DURATION_PER_CHAR * context.getResources().getText(msg).length();
        Crouton crouton = Crouton.makeText(context,
                msg,
                new Style.Builder().setConfiguration(new Configuration.Builder().setDuration(duration).build())
                        .setBackgroundColorValue(context.getResources().getColor(R.color.light_blue))
                        .setPaddingInPixels(padding)
                        .setHeight(LayoutParams.WRAP_CONTENT)
                        .build()
        );
        crouton.show();
        return crouton;
    }

    public static Crouton info(final Activity context, String msg) {
        final int padding = (int) context.getResources().getDimension(R.dimen.croutonPadding);
        final int duration = DURATION_PER_CHAR * msg.length();
        Crouton crouton = Crouton.makeText(context,
                msg,
                new Style.Builder().setConfiguration(new Configuration.Builder().setDuration(duration).build())
                        .setBackgroundColorValue(context.getResources().getColor(R.color.light_blue))
                        .setPaddingInPixels(padding)
                        .setHeight(LayoutParams.WRAP_CONTENT)
                        .build()
        );
        crouton.show();
        return crouton;
    }




    public static Crouton warning(final Activity context, String msg) {
        final int padding = (int) context.getResources().getDimension(R.dimen.croutonPadding);
        final int duration = DURATION_PER_CHAR * msg.length();
        Crouton crouton = Crouton.makeText(context,
                msg,
                new Style.Builder().setConfiguration(new Configuration.Builder().setDuration(duration).build())
                        .setBackgroundColorValue(context.getResources().getColor(R.color.venetian_red))
                        .setPaddingInPixels(padding)
                        .setHeight(LayoutParams.WRAP_CONTENT)
                        .build()
        );
        crouton.show();
        return crouton;
    }

    public static Crouton warning(final Activity context, int msg) {
        final int padding = (int) context.getResources().getDimension(R.dimen.croutonPadding);
        final int duration = DURATION_PER_CHAR * context.getResources().getText(msg).length();
        Crouton crouton = Crouton.makeText(context,
                msg,
                new Style.Builder().setConfiguration(new Configuration.Builder().setDuration(duration).build())
                        .setBackgroundColorValue(context.getResources().getColor(R.color.venetian_red))
                        .setPaddingInPixels(padding)
                        .setHeight(LayoutParams.WRAP_CONTENT)
                        .build()
        );
        crouton.show();
        return crouton;
    }
}